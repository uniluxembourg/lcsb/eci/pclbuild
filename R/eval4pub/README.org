#+TITLE: Evaluation of PubChemLite MetFrag CSV Builds


* Introduction

  The purpose of ~eval4pub~ collection of scripts is to check newly
  built MetFrag databases based on /PubChemLite/, before they become
  public.

  The general process comprises of setting environment variables that
  serve as the input and calling ~eval4pub.R~ script which loads
  dependencies, reads inputs, runs the evaluation and writes
  diagnostics (and some files) to stderr.


* Inputs
  The following environment variables must be set:
  - PCL_FN_METFRAG_DB :: Path to file that is to be evaluated.
  - PCL_DIRSRC :: Path to the local clone of ~pubchem~ repository. It
    can be downloaded from the [[https://git-r3lab.uni.lu/eci/pubchem][official repository]].
  - PCL_FN_METFRAG_JAR :: Path to the MetFrag jar file.
  - PCL_DIRWRK :: Path to the working directory.
  - PCL_URL_ZENODO_BENCH :: URL to the benchmarking file. Currently it
    is situated at this Zenodo repository [[https://zenodo.org/record/4146957/files/PCLite_Benchmark_wMSMS_info.csv][here]].
  - PCL_FN_ZENODO_BENCH :: Only the filename part of
    ~PCL_URL_ZENODO_BENCH~. Currently,
    ~PCLite_Benchmark_wMSMS_info.csv~
  - PCL_RUN_NAME :: A string used to derive results directories and
    file names. It can be anything.
  - PCL_FUTURE_PLAN :: Package ~future~ is used to distribute the work
    and it needs to understand which concept of parallelism the user
    wants to apply. Some interesting values:
    + sequential :: Classical sequential execution.
    + multiprocess :: Shared memory parallelism (suitable for single
      node computation, ie normal multicore workstations). Works both
      on GNU\Linux and Windows.
    For other supported concurrency paradigms, see documentation of R
    package ~future~.
  - PCL_FUTURE_NWORKERS :: This is an integer allowing one to set the
   number of threads/processes which will run MetFrag in
   parallel. Ignored if the plan is ~sequential~.
  - PCL_FN_JAVA_BIN :: Path to java binary.
  - PCL_DIR_TRACK :: This is the path to CSV files where quality
    tracking information is going to be written.
  - PCL_TIME :: Date/time of CSV creation (YYYY-MM-DD
    [HH:MM:SS]). Time of the day is optiona;. If the variable is left
    undefined, then the current time is going to be used.

  As an example, see the bash script used during the PCL build process,
  #+BEGIN_SRC sh

  PCL_EVALSCRIPT=/somewhere/pubchem/pubchemlite/R/eval4pub/eval4pub.R
  export PCL_DIRSRC=/somewhere/pubchem
  export PCL_FN_METFRAG_JAR=/mnt/SCRATCH/bin/MetFrag-current.jar
  export PCL_DIRWRK=jan31_tk
  export PCL_FN_METFRAG_DB=/somewhere/CompoundDBs/MetFrag_CSVs/PubChemLite_exposomics_20210101.csv
  export PCL_URL_ZENODO_BENCH="https://zenodo.org/record/4146957/files/PCLite_Benchmark_wMSMS_info.csv"
  export PCL_FN_ZENODO_BENCH="PCLite_Benchmark_wMSMS_info.csv"
  
  export PCL_RUN_NAME="exposomics_$(date +%d%b%Y -r $PCL_FN_METFRAG_DB)_MergedBenchmark"
  export PCL_FUTURE_PLAN="multiprocess"
  export PCL_FUTURE_NWORKERS=8
  export PCL_FN_JAVA_BIN=$(which java)

  export PCL_DIR_TRACK="somewhere/here/tracme.csv"
  Rscript --vanilla "$PCL_EVALSCRIPT" 

  #+END_SRC

* Dependencies
  R packages needed for evaluation are:
  - devtools
  - RMassBank
  - ReSOLUTION
  - RChemMass
  - future
  - processx
  - assertthat

* How to Run
  
  So, to initiate the evaluation, install the dependencies, clone the
  ~pubchem~ repository, then set the environment variables in some way
  (for example using Bash or R script [ ~Sys.setenv~ function ]), then
  call ~eval4pub.R~ script.
