#!/bin/sh

# This script has to run in the directory containing the pclbuild repo dir.
# Usage:
#        ./wrap-cmd.sh getter.sh some/top/dir #or
#        ./wrap-cmd.sh build.sh  some/top/dir

TOPDIR=$2
cd "$2"
export TOPDIR=${PWD}
export PCLBUILD=${TOPDIR}/pclbuild

export INPUTURL=https://gitlab.lcsb.uni.lu/eci/tst_inp_pcl.git
export METFRAGBIN=/usr/local/bin/metfrag-current.jar
export MFJOBS=8

COMMAND=$1

mkdir -p "$TOPDIR"
mkdir -p "$TOPDIR/logs"
mkdir -p "$TOPDIR/work"

stamp=`date +%Y%m%d`

guix shell -c12 --pure -E TOPDIR -E "PCL.*" -E INPUTURL -E METFRAGBIN -E MFJOBS \
     -m "$PCLBUILD/variae/foundation.scm" \
     -m "$PCLBUILD/variae/pcl-dep-1.scm" \
     -m "$PCLBUILD/variae/pcl-dep-2.scm" \
     -- "$PCLBUILD/variae/$COMMAND" 1>>"$COMMAND.$stamp.out" 2>>"$COMMAND.$stamp.err"
