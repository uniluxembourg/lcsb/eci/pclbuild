(use-modules (guix packages)
	     (guix inferior)
	     (guix channels)
	     (guix profiles)
	     (guix utils)
	     (guix download)
	     (guix build-system gnu)
	     ((guix licenses) #:prefix license:)
	     (gnu packages)
	     (gnu packages commencement)
	     (gnu packages autotools)
	     (gnu packages gcc)
	     (gnu packages perl)
	     (gnu packages mpi)
	     (gnu packages ssh)
	     (gnu packages gawk)
	     (gnu packages base)
             (srfi srfi-1))

;; (define channels
;;   ;; This is the old revision from which we want to
;;   ;; extract guile-json.
;;   (list (channel
;;          (name 'guix)
;;          (url "https://git.savannah.gnu.org/git/guix.git")
;;          (commit "a0178d34f582b50e9bdbb0403943129ae5b560ff"))))

;; (define inferior
;;   ;; An inferior representing the above revision.
;;   (inferior-for-channels channels))

(define specs '("bash"
		"git"
		"perl"
		;; "pypy3"
		"pigz"
		"python"
		"python-requests"
		"python-mistletoe"
		"rsync"
		"sed"
		"findutils"
		"coreutils-minimal"
		"which"))




;; (define pkgs (map (lambda (spec) (first (lookup-inferior-packages inferior spec)))
;; 		  specs))







(define manifest (specifications->manifest specs))


;(packages->manifest pkgs)

manifest


