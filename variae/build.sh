#!/bin/sh

export PYTHONPATH="$PCLBUILD:$PYTHONPATH"

python3 -m build --name exposomics --logs "$TOPDIR/logs" \
	--csv "$TOPDIR/share/CompoundDBs/PubChem/PubChemLite/v2.0/MetFrag_CSVs" \
	--input "$INPUTURL" --metfrag "$METFRAGBIN" --script "$TOPDIR/pclbuild"  \
	--jobs "$MFJOBS" --work "$TOPDIR/work"
