#!/bin/sh

export PYTHONPATH="$PCLBUILD:$PYTHONPATH"

python3 -m getter -i "$INPUTURL" \
--logs "$TOPDIR/logs" --to "$TOPDIR/work" --retry 180
