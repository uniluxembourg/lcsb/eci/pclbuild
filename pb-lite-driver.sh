#!/bin/bash

declare -agx ALL_STAGES=( "cleanup" "prepare" "fetch_scripts_sources" "build" "quotes" "eval4pub" "epilog")


# Initialisation.
function prolog() {
    trap "exit 1" SIGUSR1
    TOPPID="$$"
    fatal(){
	say "(CRITICAL)" "$@"
	kill -s SIGUSR1 "$TOPPID"
    }

}

function cleanup() {
    # Clean up.
    rm -vf "$WORKDIR"/*.{log,out,rpt,csv,filter,legend}
    rm -vf "$WORKDIR/index.tsv"
    rm -vf "$WORKDIR/cid-bits.tab.gz"
}

function prepare() {
    # GENERATE BIT AND LEGEND INPUTS
    gen_filtfile "$MAPFILE" "$FILTERFILE"
    gen_legend "$MAPFILE" "$LEGENDFILE"
    
    # Write basic build info to the log file.
    stamp

}

fetch_scripts_sources() {
    # Adapt paths.
    source "${SCRIPTDIR}/adapt_scripts"

    # Copy CCS source.
    rsync -avzz  "$CCS_SRC_PATH" .
}

function build() {
    # BUILD
    header BUILD
    source "${SCRIPTDIR}/generate" "$FILTERFILE" "$LEGENDFILE"
    footer BUILD
}

function buildccs() {
    header BUILDCCS
    fccs=`python3 "${SCRIPTDIR}/manif_val.py" ccs_src_path`;
    fccs=`basename "$fccs"`;
    Rscript --vanilla "${SCRIPTDIR}/R/add_ccs2csv.R" "$fccs" "$WORKDIR/$OUTMFFILE" "$WORKDIR/$OUTMFCCSFILE"
    footer BUILDCCS
}

function quotes() {
    header FIX TRIPLE QUOTES IN MZ
    ${SCRIPTDIR}/fix_mass_quotes.R "$WORKDIR/$OUTMFFILE"
    [ "$?" -ne 0 ] && fatal "(quotes) Could not fix the quotes."
    footer FIX TRIPLE QUOTES IN MZ
}

function epilog() {
    # Dump WORKDIR to BACKUPDIR.

    header BACK WORKDIR UP $(date) 
    if [ "$DOBACKUP" = "yes" ]; then
	rsync -avz --exclude "*.bak" --exclude "*.log" --exclude "*.log.[0-9]*" --log-file="${WORKDIR}/build.transfer.log"  "${WORKDIR}/" "$BACKUPDIR"
	[ "$?" -ne 0 ] && fatal "(epilog) Could not back up $WORKDIR to $BACKUPDIR."
    else
	say "(epilog) Backup was skipped as requested."
    fi
    footer BACK WORKDIR UP $(date)


    # COMMIT METFRAG CSVs
    header COMMIT METFRAG CSV
    rsync -avz "$WORKDIR/$OUTMFFILE" "$MFDIR"
    [ "$?" -ne 0 ] && fatal "(epilog) Could not transfer $OUTFMFILE to $MFDIR."
    footer COMMIT METFRAG CSV

    # COMMIT METFRAG SUMMARIES
    header COMMIT METFRAG SUMMARY
    rsync -avz "$WORKDIR/$OUTMFSUMM"* "$MFSUMMDIR"
    [ "$?" -ne 0 ] && fatal "(epilog) Could not transfer $OUTMFSUMM files to $MFSUMMDIR."
    footer COMMIT METFRAG SUMMARY


    say "********** PubChem Lite LOG END $(date) **********"

    rsync -avz "$LOGDIR" "${MFDIR}/build_log"

}

# * Program Starts Here

prolog

# ** Control loop.
[ -z "$1" ] && usage && exit -1
while [ ! -z "$1" ]; do
  case "$1" in
     --input|-i)
         shift
	 INPUTDIR="$1"
         ;;
     --stage|-s)
         shift
	 nstages=${#STAGES[@]}
	 curr=$(($nstages+1))
	 STAGES[$nstages+1]="$1"
         ;;
     --all-stages|-a)
         shift
	 curr=0
	 for st in "${ALL_STAGES[@]}"; do
	     STAGES[$curr]="$st"
	     curr=$(($curr+1))
	 done	     
         ;;
     *)
        usage
        ;;
  esac
shift
done

# # ** Stages to run.

source "${SCRIPTDIR}/library.sh" # Get the additional functionality.

# ** Execute stages.
for stage in "${STAGES[@]}";do
    case "$stage" in
	"cleanup")
	    cleanup
	    ;;
	"prepare")
	    prepare
	    ;;
	"fetch_scripts_sources")
	    fetch_scripts_sources
	    ;;
	"build")
	    build
	    ;;
	"quotes")
	    quotes
	    ;;
	"buildccs")
	    buildccs
	    ;;
	"eval4pub")
	    header EVAL4PUB CHECK
	    . "${SCRIPTDIR}/eval4pub.sh"
	    footer EVAL4PUB CHECK
	    ;;
	"epilog")
	    epilog
	    ;;
	*)
	    echo "Unknown stage: $stage" >2
	    ;;
    esac
done
    

