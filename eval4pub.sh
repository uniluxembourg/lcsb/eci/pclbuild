# -*- shell-script -*-

. "$SCRIPTDIR/library.sh"

say "(eval4pub): PCL_DIRSRC = $PCL_DIRSRC"
mkdir -p "$PCL_DIR_TRACK"
PCL_FN_METFRAG_JAR="$PCL_FN_METFRAG_JAR" \
		  PCL_URL_ZENODO_BENCH="$PCL_URL_ZENODO_BENCH" \
		  PCL_FN_ZENODO_BENCH=$(basename "$PCL_URL_ZENODO_BENCH") \
		  Rscript --vanilla "$PCL_EVALSCRIPT"
