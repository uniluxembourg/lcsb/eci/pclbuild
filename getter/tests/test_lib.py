# Copyright (C) 2023 by University of Luxembourg
import unittest
import os
import shutil
import tempfile
import hashlib
import lib
from unittest.mock import patch

# All test cases have been written with the assistance of ChatGPT 4.

class TempTest(unittest.TestCase):
    def setUp(self):
        self.original_dir = os.getcwd()
        self.test_dir = tempfile.mkdtemp()
        os.chdir(self.test_dir)
        
    def tearDown(self):
        shutil.rmtree(self.test_dir)
        os.chdir(self.original_dir)


class TestCompareMd5(unittest.TestCase):
    def test_compare_md5_with_matching_md5_file(self):
        # Create a temporary file with known content
        content = b"Hello, world!"
        filename = "test_file.txt"
        with open(filename, "wb") as f:
            f.write(content)

        # Compute the MD5 hash of the temporary file
        expected_md5 = hashlib.md5(content).hexdigest()

        # Write the expected MD5 hash to a file
        md5_filename = filename + ".md5"
        with open(md5_filename, "w") as f:
            f.write(expected_md5)

        # Call the function being tested
        result = lib.compare_md5(filename)

        # Check the result
        self.assertTrue(result)

        # Clean up the temporary files
        os.remove(filename)
        os.remove(md5_filename)

    def test_compare_md5_with_non_matching_md5_file(self):
        # Create a temporary file with known content
        content = b"Hello, world!"
        filename = "test_file.txt"
        with open(filename, "wb") as f:
            f.write(content)

        # Compute the MD5 hash of the temporary file
        expected_md5 = hashlib.md5(content).hexdigest()

        # Write a different MD5 hash to a file
        md5_filename = filename + ".md5"
        with open(md5_filename, "w") as f:
            f.write("not the expected MD5 hash")

        # Call the function being tested
        result = lib.compare_md5(filename)

        # Check the result
        self.assertFalse(result)

        # Clean up the temporary files
        os.remove(filename)
        os.remove(md5_filename)

    def test_compare_md5_with_missing_md5_file(self):
        # Create a temporary file with known content
        content = b"Hello, world!"
        filename = "test_file.txt"
        with open(filename, "wb") as f:
            f.write(content)

        # Call the function being tested
        result = lib.compare_md5(filename)

        # Check the result
        self.assertFalse(result)

        # Clean up the temporary files
        os.remove(filename)

class TestReadLinesFromFile(unittest.TestCase):
    def setUp(self):
        # Create a temporary file for testing
        self.tmp_file = tempfile.NamedTemporaryFile(mode='w', delete=False)
        self.tmp_file.write('line 1\nline 2\nline 3\n')
        self.tmp_file.close()

    def tearDown(self):
        # Delete the temporary file after testing
        os.remove(self.tmp_file.name)

    def test_read_lines_from_file(self):
        # Call the read_lines_from_file function with the temporary file
        lines = lib.read_lines_from_file(self.tmp_file.name)

        # Check that the lines are read correctly
        expected_lines = ['line 1\n', 'line 2\n', 'line 3\n']
        self.assertEqual(lines, expected_lines)

if __name__ == '__main__':
    unittest.main()

        
