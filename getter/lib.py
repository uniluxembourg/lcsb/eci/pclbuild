# Copyright (C) 2023 by University of Luxembourg

import configparser
import fcntl
import hashlib
import logging as log
import os
import pathlib
import socket
import ssl
import sys
import time
import urllib.request


from .consts import DIR_INPUT, HASH_CHUNK,           \
    FN_LOCK_GETTER, FN_REBUILD_NEEDED, FN_SOURCES

import aux

# Functions in this module written with the assistance of ChatGPT 4:
#
# - compare_md5
# - read_lines_from_file
# - find_changed_files
# - download_files
#
# In the meantime a human (Todor) changed them beyond recognition.


log = aux.logger()

def calc_md5(filename):
    # Compute the MD5 hash of the file
    with open(filename, "rb") as f:
        file_hash = hashlib.md5()
        while chunk := f.read(HASH_CHUNK):
            file_hash.update(chunk)
        file_md5 = file_hash.hexdigest()

    return(file_md5)


def compare_md5(filename):

    # Calculate MD5.
    file_md5 = calc_md5(filename)

    # Read the MD5 hash from the .md5 file
    md5_filename = filename + ".md5"
    if not os.path.exists(md5_filename):
        return False
    with open(md5_filename, "r") as f:
        expected_md5 = f.read().strip().split()[0]

    # Compare the computed and expected MD5 values
    return file_md5 == expected_md5


def signal_rebuild_needed(workdir):
    try:
        fn = os.path.join(workdir,FN_REBUILD_NEEDED)
        pathlib.Path(fn).touch()
    except Exception as e:
        log.critical(e)
        exit(1)

def check_rebuild_needed(workdir):
    try:
        fn = os.path.join(workdir,FN_REBUILD_NEEDED)
        return(os.path.exists(fn))
    except Exception as e:
        log.critical(e)
        exit(1)

def signal_rebuild_done(workdir):
    try:
        fn = os.path.join(workdir,FN_REBUILD_NEEDED)
        pathlib.Path(fn).unlink(missing_ok=True)
    except Exception as e:
        log.critical(e)
        exit(1)


def find_changed_indices(filenames):
    indices = []
    for k in range(0,len(filenames)):
        fn = filenames[k]
        if not os.path.exists(fn) or not compare_md5(fn):
            indices.append(k)
                   
    return(indices)



def download_files(urls,pause,silent=True):
    """
    Downloads a list of files from their URLs and saves them to the specified output directory.

    Args:
    - urls (list of str): URLs of files to be downloaded
    - dir (str): Path to the directory where downloaded files should be saved

    Returns:
    - None
    """

    def download_one_file(url):
        try:
            filename = url.split('/')[-1]
            if not silent:
                log.info("Begin downloading: " + filename)
            urllib.request.urlretrieve(url, filename)
            if not silent:
                log.info("Finished downloading: " + filename)
            return(True)
    
        except urllib.error.URLError as e:
            log.error(f"{url} (URL error): {e}")
            return(False)
        except urllib.error.HTTPError as e:
            log.error(f"{url} (HTTP error): {e}")
            return(False)
        except socket.error as e:
            log.error(f"{url} (socket error): {e}")
            return(False)
        except ConnectionResetError as e:
            log.error(f"{url} (connection reset error): {e}")
            return(False)
        except ssl.SSLError as e:
            log.error(f"{url} (SSL error): {e}")
            return(False)
        except Exception as e:
            log.error(f"{url} (unhandled error): {e}")
            return(False)




    status = False
    for url in urls:
        while not status:
            status = download_one_file(url)
            time.sleep(pause)
        status = False
        
    return(None)


def download_hashes(urls,pause):
    hash_urls = [ f + ".md5" for f in urls ]
    download_files(hash_urls, pause=pause)
    
        
def read_lines_from_file(fn):
    """
    Reads a list of lines from a text file.

    Args:
    - fn (str): Path to the text file

    Returns:
    - List of str: List of lines in the text file
    """

    try:
        with open(fn, 'r') as file:
            lines = file.readlines()
    except OSError as e:
        log.critical(f"OS error occurred: {e}")
    except TypeError as e:
        log.critical(f"Type error occurred: {e}")
    except ValueError as e:
        log.critical(f"Value error occurred: {e}")
    
    return [ l.strip() for l in lines ]


def lock(fn):
    lock_file = open(fn, 'w')

    # Try to acquire the lock.
    try:
        fcntl.flock(lock_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
        # log.info("Lock acquired.")
    except IOError as e:
        if e.errno == 11:  # Errno 11 is EAGAIN (Resource temporarily unavailable).
            lock_file.close()
            exit(0)
        else:
            # Another IO error occurred.
            log.critical(f"Encountered error while trying to get a lock: {e}")
            exit(1)
                
    return(lock_file)


def lock_block(fn):
    lock_file = open(fn, 'w')

    # Try to acquire the lock.
    try:
        fcntl.flock(lock_file, fcntl.LOCK_EX)
        # log.info("Lock acquired.")
    except Exception as e:
        log.critical(f"Encountered error while trying to get a lock: {e}")
        exit(1)
                
    return(lock_file)


def unlock(lock_file):
    try:
        fcntl.flock(lock_file, fcntl.LOCK_UN)
        lock_file.close()
        # log.info("Lock released.")
    except IOError as e:
        log.critical("Unable to unlock.")
        exit(1)

    
def switch_to_dwnl_dir(dwnl_dir):
    try:
        if not os.path.exists(dwnl_dir):
            log.info('Download directory does not exist, trying to create it.')
            try:
                os.mkdir(dwnl_dir)
            except OSError as e:
                log.critical(f"OS error occurred: {e}")
                exit(1)
            except TypeError as e:
                log.critical(f"Type error occurred: {e}")
                exit(1)
            except ValueError as e:
                log.critical(f"Value error occurred: {e}")
                exit(1)

        os.chdir(dwnl_dir)
    except OSError as e:
        log.critical(f"OS error occurred: {e}")
        exit(1)
    except TypeError as e:
        log.critical(f"Type error occurred: {e}")
        exit(1)
    except ValueError as e:
        log.critical(f"Value error occurred: {e}")
        exit(1)



def get_dest_files(urls):
    files = []
    for url in urls:
        filename = url.split('/')[-1]
        files.append(filename)
    return(files)



        
def run(fn_input_urls,dwnl_dir,retry_in_sec):
    """
    Reads urls of the input files for the PCL build from a text file,
    compares their md5 hashes (which should exist appropriately named
    on the server) with those in the download directory and downloads
    those that changed.

    Args:
    - fn_input_urls: A `txt` file containing a list of urls for files
      that should be downloaded.
    - dwnl_dir: The download directory.
    """


    def run_internal():

        # Read in the sources.
        urls = read_lines_from_file(fn_input_urls)


        # Get hashes from the server.
        download_hashes(urls, pause = retry_in_sec)

        dest_files = get_dest_files(urls)

        # Which files have to be downloaded?
        indices = find_changed_indices(dest_files)

        # Resource to files differing from the server.
        urls_to_download = [urls[k] for k in indices]

        # Download through errors, connection losses and other stuff.
        if (len(urls_to_download)>0):
            log.info('Download directory is: ' + dwnl_dir)
            log.info("List of changed files: ")
            for u in urls_to_download:
                log.info(f" - {u}")

        download_files(urls_to_download,
                       pause = retry_in_sec,
                       silent = False)

        if (len(urls_to_download)>0):
            signal_rebuild_needed(os.getcwd())
            log.info("Signaled that rebuild is needed.")
        
        
        return(None)

    
    try:
        retry_in_sec = float(retry_in_sec)
    except Exception as e:
        log.critical("Argument retry_in_sec must be convertable to float.")
        exit(1)

    # Note the starting directory.
    olddir = os.getcwd()
    try:
        # Switch to download directory.
        switch_to_dwnl_dir(dwnl_dir)

        # Obtain a lock to prevent parallel getter runs.
        lock_file = lock(FN_LOCK_GETTER)
        
        log.info("Module `getter' reports for duty.") 

        # Main procedure.
        run_internal()

        # Unlock what needs to be unlocked.
        unlock(lock_file)

        
    except Exception as e:
        log.critical("Unhandled error. Cannot continue.")
        log.critical(e)

    finally:
        # Return to the original directory.
        os.chdir(olddir)



