import datetime

# Copyright (C) 2023 by University of Luxembourg


# Input directory.
DIR_INPUT = "input"


# The file chunk size used while calculating the MD5 hashes.
HASH_CHUNK = 8192

# Lock file name for getter.
FN_LOCK_GETTER = "getter.lock"

# Base name for the log.
FN_LOG = "getter.log"

# Existence of this file signals builder to start a rebuild.
FN_REBUILD_NEEDED = ".REBUILD_NEEDED"

# Name of the file containing input file URLs to be downloaded from PubChem.
FN_SOURCES = "sources.txt"

# Log time.
LOG_WHEN='W6'
LOG_ATTIME=datetime.time(0,0,0) # Rotate at midnight on LOG_WHEN day.
# Three months worth of logs kept on the system.
LOG_HOWMANY=12
