# Control the process of uploading to Zenodo

import os
import sys
import ruamel.yaml as yaml
import json
import logging
from  pathlib import Path
logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)



pcl_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(pcl_path+'/depositor')
import zenodo as zen

logging.info("Started Depositor.")
s=zen.init_req_session() 

zenodo_token=os.environ['PCL_ZENODO_TOKEN']
fn_input=os.environ['PCL_MANIF']
logging.info("Input file path: %s", fn_input)
with open(fn_input,'r') as fhandle:
    conf=yaml.load(fhandle, Loader=yaml.Loader)


depoconf=conf['depositor']
depoconf['access_token']=zenodo_token
dir_dpr=depoconf['local']

# Give up if DONOTRUN present.
if os.path.exists(dir_dpr+'/'+'DONOTRUN'):
    logging.info('DONOTRUN condition detected. Exiting.')
    sys.exit(0)

# Prevent further runs, until allowed.
Path(dir_dpr+'/'+'DONOTRUN').touch()
url_zenodo=depoconf['url_zenodo']
fn_publish=dir_dpr+'/publish.md'
dir_pub=dir_dpr+'/publish'

# First, read publish.md.
meta,content=zen.parse_publish(fn_publish)

#  For debugging.
with open('content.json','w',encoding="utf-8") as f:
    print(json.dumps(content,indent=4),file=f)

with open('meta.json','w') as f:
    print(json.dumps(meta,indent=4),file=f)

with open('description.txt','w') as f:
    print(meta['description'],file=f)

ver_what='donothing'

# First, determine if we have something already there.
jdepo = zen.depo_query(s,depoconf,meta)




if not jdepo:
    # If not, we should create it.
    jdepo = zen.depo_create(s,depoconf,meta)
    with open('fresh_depo.json','w') as f:
        print(json.dumps(jdepo,indent=4),file=f)
    ver_what='create'
else:
    # If there is a deposition, make sure it's clean.
    jdepo = zen.depo_discard_edit(s,depoconf,meta,jdepo)


# Which files have changed (including newly added)?
files = zen.depo_files_query(s,depoconf,jdepo)

if len(files) != 0:
    # Create a new version.
    jdepo,meta=zen.depo_files_upload(s,depoconf,meta,jdepo,files)
    if ver_what == 'donothing':
        ver_what='update'

if ver_what == 'donothing':
    ver_what='keepold'

meta = zen.depo_semver_stamp(meta,jdepo,what=ver_what) # Retain previous
                                                       # version if only
                                                       # editing and the
                                                       # previous version
                                                       # exists, othervise
                                                       # '0.1.0'.

# Editing metadata (if needed).
# Unlock deposition for editing, if needed.
jdepo = zen.depo_edit(s,depoconf,meta,jdepo)
jdepo = zen.depo_update_meta(s,depoconf,meta,jdepo) # Update remote metadata.


    
    




jdepo = zen.depo_publish(s,depoconf,meta,jdepo)



None
