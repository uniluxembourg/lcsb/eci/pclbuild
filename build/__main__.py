import argparse
import logging as log
import os
import sys

from . import lib

import aux


from .consts import FN_LOG, LOG_WHEN, LOG_ATTIME, LOG_HOWMANY

# Get logger.
log = aux.logger()


# We want to throw errors so that I can predictably log them.
class MyArgumentParser(argparse.ArgumentParser):
    def exit(self, status=0, message=None):
        if status:
            raise Exception(f'Exiting because of an error: {message}')
        exit(status)


parser = MyArgumentParser(description='Build PubChemLite database.',epilog="")

parser.add_argument('--csv', '-c',
                    help="Directory for storing MetFrag CSVs.",
                    required=True)
parser.add_argument('--input', '-i',
                    help="URL of a git repo containing configuration files for `builder' .",
                    required=True)
parser.add_argument('--jobs','-j',help="Number of concurrent processes for various purpouses",
                    default="2")

parser.add_argument('--logs','-l',help="Directory which should contain logs. If not specified, logs will be created in logs subdirectory of the current working directory.",default=None)

parser.add_argument('--metfrag',
                    '-m',
                    help="Path to MetFrag jar.",
                    required=True)
parser.add_argument('--name',
                    '-n',
                    help="Name of the run (for example, exposomics).",
                    required=True)

parser.add_argument('--nobackup',
                    '-x',
                    help="Skip backup",
                    action='store_true')

parser.add_argument('--phase',
                    '-p',
                    help="Which phase to run. Default is all phases.",
                    action="append",default=[])
parser.add_argument('--scripts',
                    '-s',
                    help="Directory which contains scripts needed for the build.",
                    required=True)
parser.add_argument('--work',
                    '-t',
                    help="Build process runs in this directory.",
                    required=True)




                    

args = parser.parse_args()

# Initialise the logger.
log = aux.conf_logger(logr=log,dir_log=args.logs,
                      fn_log=FN_LOG,descriptor='build',
                      when=LOG_WHEN,atTime=LOG_ATTIME,
                      backupCount=LOG_HOWMANY)


lib.main(args)
