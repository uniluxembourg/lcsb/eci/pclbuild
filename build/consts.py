import datetime

FN_EVAL4PUB = "eval4pub.R"
FN_LOCK_BUILD = "build.lock"

# Base name for the log.
FN_LOG = "build.log"

FN_MANIFEST = "manifest.yaml"
FN_SCRIPT = "pb-lite-driver.sh"
DIR_BACKUP = "backup"
DIR_EVAL4PUB = "eval4pub"
DIR_INPUT = "build_inputs"
DIR_MF_SUMMS = "MetFragSummaries"

CMD_CMPR_N = 4
CMD_CMPR = f"pigz -p {CMD_CMPR_N}"
CMD_DCMPR = f"pigz -p {CMD_CMPR_N} --decompress"

# Log time.
LOG_WHEN='W6'
LOG_ATTIME=datetime.time(0,0,0) # Rotate at midnight on LOG_WHEN day.
# Three months worth of logs kept on the system.
LOG_HOWMANY=12
