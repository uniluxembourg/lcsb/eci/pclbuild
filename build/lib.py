import logging as log
import os
import subprocess
import time
import yaml

from . import lib
from .consts import CMD_CMPR, CMD_DCMPR, LOG_WHEN, LOG_HOWMANY, DIR_BACKUP, \
                    DIR_EVAL4PUB, DIR_INPUT, DIR_MF_SUMMS, FN_EVAL4PUB, \
                    FN_LOCK_BUILD, FN_LOG, FN_MANIFEST, \
                    FN_SCRIPT

import aux
import getter.lib


log = aux.logger()



def process_manif(dirs,files):
    with open(files['manif'], "r") as f:
        manif = yaml.load(f, Loader=yaml.FullLoader)

    files['map'] = os.path.join(dirs['input'],manif['map'])

    try:
        assert os.path.exists(files['map']), f"Map {files['map']} must exist in the input directory."
    except AssertionError as e:
        log.critical(e)
        exit(1)

    nm = os.path.basename(os.path.splitext(files['map'])[0])
    tstamp = time.strftime("%Y%m%d")
    
    files['legend'] = os.path.join(dirs['work'],nm + '.legend')
    files['filter'] = os.path.join(dirs['work'],nm + '.filter')
    files['outmffile'] = nm + "_" + tstamp + ".csv"
    files['outmfclone'] = nm + ".csv"
    files['outmfccsfile'] = nm + "_" + "CCSBase_" + time.strftime("%Y%m%d") + ".csv"
    files['outmfccsclone'] = nm + "_" + "CCSBase" + ".csv"
    files['outmfsumm'] = "MetFragPubChemLiteSummary_" + tstamp

    # ccs_src_path is relative to the starting directory.
    files['ccs_src_path'] = os.path.join(dirs['start'],manif['ccs_src_path'])
    try:
        assert os.path.exists(files['ccs_src_path']), f"Unable to find the CCS file at {files['ccs_src_path']}. Please  check the manifest {files['map']}"
    except AssertionError as e:
        log.critical(e)
        exit(1)

    
    dirs['top_backup'] = os.path.join(dirs['start'],manif['top_backup'])
    dirs['metfrag'] = os.path.join(dirs['start'],manif['metfrag'])
    dirs['metfrag_summaries'] = os.path.join(dirs['start'],manif['metfrag'],DIR_MF_SUMMS)


    urls = {}
    urls['bench'] = manif['url_bench']
        
    return(dirs, files, urls)
    

def process_args(args):
    dirs = {}
    files = {}

    dirs['csv'] = args.csv
    
    dirs['work'] = args.work
    dirs['scripts'] = args.scripts
    dirs['start'] = os.getcwd()

    try:
        dirs['input'] = os.path.join(dirs['work'],DIR_INPUT)
    except Exception as e:
        log.critical(e)
        exit(1)

    # Get input.
    aux.clone(dest=dirs['input'],url=args.input)
    
    files['mf'] = args.metfrag
    files['build'] = os.path.join(dirs['scripts'],FN_SCRIPT)
    files['manif'] = os.path.join(dirs['input'],FN_MANIFEST)
    
    dirs = { kd:os.path.abspath(dirs[kd]) for kd in dirs.keys() }
    files = { kf:os.path.abspath(files[kf]) for kf in files.keys() }

    
    for k in dirs.keys():
        if not os.path.exists(dirs[k]):
            log.critical(f"({k}): Path {dirs[k]} does not exist or is unreachable.")
            exit(1)

    for k in files.keys():
        if not os.path.exists(files[k]):
            log.critical(f"({k}): Path {files[k]} does not exist or is unreachable.")
            exit(1)

    dirs['input'] = os.path.join(dirs['work'],DIR_INPUT)

    dirs['log'] = args.logs
    files['log'] = os.path.join(args.logs,FN_LOG)

    aux.clone(dest=dirs['input'],url=args.input)
    
    dirs,files,urls = process_manif(dirs,files)

    dirs['track'] = os.path.join(dirs['metfrag'],DIR_EVAL4PUB)
    
    try:
        assert os.path.exists(dirs['metfrag']), f"MetFrag DB location (in manifest.yaml `metfrag' must exist and be reachable."
    except AssertionError as e:
        log.critical(e)
        exit(1)

    try:
        assert os.path.exists(dirs['top_backup']), f"Top backup dir (in manifest.yaml `top_backup' must exist and be reachable."
    except AssertionError as e:
        log.critical(e)
        exit(1)

    dir_backup = f"{DIR_BACKUP}_{args.name}_{time.strftime('%Y%m%d_%H_%M_%S')}"
    dirs['backup'] = os.path.join(dirs['top_backup'],dir_backup)

    try:
        os.mkdir(dirs['backup'])
    except FileExistsError as e:
        log.critical(f"The backup dir {dirs['backup']} exists. Aborted.")
        exit(1)

    try:
        os.mkdir(dirs['track'])
    except FileExistsError as e:
        pass
    except Exception as e:
        log.critical(e)
        exit(1)

    try:
        os.mkdir(dirs['metfrag_summaries'])
    except FileExistsError as e:
        pass
    except Exception as e:
        log.critical(e)
        exit(1)

    files['pcl_fn_metfrag_db'] = os.path.join(dirs['work'],'PubChemLite_'+args.name+'_[0-9]*.csv')

    files['pcl_evalscript'] = os.path.join(dirs['scripts'],'R', DIR_EVAL4PUB, FN_EVAL4PUB)


    phases = []
    if (len(args.phase)==0):
        phases = ["-a"]
    else:
        for a in args.phase:
            phases.append("-s")
            phases.append(a)

    return(dirs,files,urls,phases)

def shell_environment(dirs,files,urls,args):

    # Note that there are several redundant dirs['scripts']
    # env. vars. Should be taken care of at some point. :TODO:

    # Compression.
    os.environ['CMPR'] = CMD_CMPR
    os.environ['DCMPR'] = CMD_DCMPR


    # Some paths earlier in param.sh.
    os.environ['BACKUPDIR'] = dirs['backup']
    os.environ['DDIR'] = dirs['scripts']
    os.environ['INPUTDIR'] = dirs['input']
    os.environ['LOGDIR'] = dirs['log']
    os.environ['MFDIR'] = dirs['metfrag']
    os.environ['MFSUMMDIR'] = dirs['metfrag_summaries']
    os.environ['SCDIR'] = dirs['scripts']
    os.environ['SCRIPTDIR'] = dirs['scripts']
    os.environ['STARTDIR'] = dirs['start']
    os.environ['WORKDIR'] = dirs['work']

    # Some files, mostly based on the manifest.
    os.environ['CCS_SRC_PATH'] = files['ccs_src_path']
    os.environ['FILTERFILE'] = files['filter']
    os.environ['LOGFILE'] = files['log']
    os.environ['MANIF'] = files['manif']
    os.environ['MAPFILE'] = files['map']
    os.environ['LEGENDFILE'] = files['legend']
    os.environ['OUTMFCCSFILE'] = files['outmfccsfile']
    os.environ['OUTMFCLONE'] = files['outmfclone']
    os.environ['OUTMFCCSCLONE'] = files['outmfccsclone']
    os.environ['OUTMFFILE'] = files['outmffile']
    os.environ['OUTMFSUMM'] = files['outmfsumm']

    # The 'PCL_' env. vars.
    os.environ['PCL_DIR_TRACK'] = dirs['track']
    os.environ['PCL_DIRSRC']=dirs['scripts']
    os.environ['PCL_DIRWRK']=dirs['work']
    os.environ['PCL_EVALSCRIPT'] = files['pcl_evalscript']
    os.environ['PCL_FN_METFRAG_DB'] = os.path.basename(files['outmffile'])
    os.environ['PCL_FN_METFRAG_JAR'] = files['mf']
    os.environ['PCL_FUTURE_NWORKERS'] = args.jobs
    if args.jobs != "1":
        os.environ['PCL_FUTURE_PLAN'] = "multisession"
    else:
        os.environ['PCL_FUTURE_PLAN'] = "sequential"
        

    os.environ['PCL_RUN_NAME'] = args.name
    os.environ['PCL_URL_ZENODO_BENCH'] = urls['bench']
    


    # Do we back up?
    if not args.nobackup:
        os.environ['DOBACKUP']="yes"
    
    # Number of jobs.
    os.environ['NTHREAD'] = args.jobs

    # Log parameters.
    os.environ['LOGWHEN'] = LOG_WHEN
    os.environ['LOGHOWMANY'] = str(LOG_HOWMANY)

def main(args):

    log.info("Module `build' reporting for duty.")

    dirs, files, urls, phases = process_args(args)
    

    try:
        lock_file = None
        lock_file2 = None
        os.chdir(dirs['work'])
        # Exit if there is another build process running.
        lock_file = getter.lib.lock(FN_LOCK_BUILD)
        
        # Wait until getter finishes, then prevent further getter runs
        # until build is done.
        lock_file2 = getter.lib.lock_block(getter.consts.FN_LOCK_GETTER)

        # Check if getter informed us if we should build.
        if not getter.lib.check_rebuild_needed(dirs['work']):
            log.info("No inputs changed, exiting.")
            exit(0)

        
    except OSError as e:
        log.critical(e)
        exit(1)

    try:
        shell_environment(dirs=dirs,
                          files=files,
                          urls=urls,
                          args=args)

        # Build the command line list.
        phases.insert(0,files['build'])
         # Run the command. Error is raised if it fails.
        subprocess.run(phases,check=True)

        # If the command completes successfully, finalise the build.
        getter.lib.signal_rebuild_done(dirs['work'])

    except subprocess.CalledProcessError as e:
        log.critical(f"Script {FN_SCRIPT} returned with an error: {e}")
        exit(1)
    except OSError as e:
        log.critical(e)
        exit(1)
    except Exception as e:
        log.critical(f"Unhandled error: {e}")
        exit(1)
    finally:
        if lock_file:
            getter.lib.unlock(lock_file)

        if lock_file2:
            getter.lib.unlock(lock_file2)
            
        os.chdir(dirs['start'])
        

            


    
