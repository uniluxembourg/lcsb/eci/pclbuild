import os
import sys
import yaml
import json
import logging

from pathlib import PurePath
dir_start=os.environ['STARTDIR']
fn_input=PurePath(dir_start,os.environ['MANIF'])

with open(fn_input,'r') as fhandle:
    conf=yaml.load(fhandle, Loader=yaml.Loader)

args=sys.argv.copy()
args.pop(0)

for key in args:
    conf=conf[key]

print(conf)

    
