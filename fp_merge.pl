#!/usr/bin/perl
#
#lrwxrwxrwx 1 bolton chemistry        45 Oct 17 03:57 CID-Parent.gz -> /am/ftp-pubchem/Compound/Extras/CID-Parent.gz
#lrwxrwxrwx 1 bolton chemistry        48 Oct 17 03:57 CID-InChI-Key.gz -> /am/ftp-pubchem/Compound/Extras/CID-InChI-Key.gz
#lrwxrwxrwx 1 bolton chemistry        43 Oct 17 03:56 CID-PMID.gz -> /am/ftp-pubchem/Compound/Extras/CID-PMID.gz
#lrwxrwxrwx 1 bolton chemistry        45 Oct 17 03:57 CID-Patent.gz -> /am/ftp-pubchem/Compound/Extras/CID-Patent.gz


print STDERR "Reading fingerprint data...\n";
my %u = ();
open( LST, "gunzip < tier0_bits.tsv.gz |" ) || die "Unable to read tier0_bits.tsv.gz!\n";
$_ = <LST>;
$_ = <LST>;
while ( $_ = <LST> ) {
  chop;
  my ( $cid, $fp ) = split( /	/, $_, 2 );
  $u{ $cid } = $fp;
}
close( LST );
my $ncid = keys( %u );
print STDERR "$ncid unique CIDs\n";

print STDERR "Getting a single best name for the representative CID\n";
my $prev = 0;
my %ipn = ();
open ( NAM, "/usr/bin/gunzip < CID-Synonym-filtered.gz |" ) || die "Unable to read CID-Synonym-filtered.gz!\n";
while ( $_ = <NAM> ) {
  chop;
  my( $c, $n ) = split( /	/, $_, 2 );
  if ( $c == $prev ) { next; }

  if ( defined( $u{ $c } ) ) {
    $ipn{ $c } = $n;
    print "Found CID $c with name: $n\n";
  } else {
    print "Missing CID $c with name: $n\n";
  }

  $prev = $c;
}
close( NAM );
my $nipn = keys( %ipn );
print STDERR "Located $nipn names for $nuuu representative CIDs\n";

exit( 1 );

#lrwxrwxrwx 1 bolton chemistry        45 Oct 17 03:57 CID-Parent.gz -> /am/ftp-pubchem/Compound/Extras/CID-Parent.gz
print STDERR "Reading parent data...\n";
open( PAR, "gunzip < CID-Parent.gz |" ) || die "Unable to read CID-Parent.gz!\n";
while( $_ = <PAR> ) {
  chop;
  my ( $cid, $pid ) = split( /	/, $_, 2 );
  if ( defined( $u{ $cid } ) ) {
    if ( $pid eq "" || $pid == 0 ) {
      $v{ $cid }{ $cid } = "";
      $vv{ $cid }{ $cid } = "";
    } else {
      $v{ $pid }{ $cid } = "";
      $vv{ $cid }{ $pid } = "";
    }
  }
}
close( PAR );

print STDERR "Backfilling CIDs without parent...\n";
my $nbf = 0;
foreach my $cid ( keys( %u ) ) {
  if ( ! defined( $vv{ $cid } ) ) {
    $v{ $cid }{ $cid } = "";  # Create "self" parent record
    $nbf++;
  }
}
my $ncid = keys( %v );
print STDERR "$ncid unique parent CIDs ($nbf backfilled)\n";


#lrwxrwxrwx 1 bolton chemistry        48 Oct 17 03:57 CID-InChI-Key.gz -> /am/ftp-pubchem/Compound/Extras/CID-InChI-Key.gz
print STDERR "Reading InChIKey data...\n";
open( INC, "gunzip < CID-InChI-Key.gz |" ) || die "Unable to read CID-InChI-Key.gz!\n";
while( $_ = <INC> ) {
  chop;
  my ( $cid, $inchi, $key ) = split( /	/, $_, 3 );
  if ( defined( $v{ $cid } ) ) {
    my ( $f, $s, $t ) = split( /-/, $key, 3 );
    $i{ $f }{ $cid } = "";
    $ii{ $cid }{ $f } = "";
  }
}
close( INC );
my $nfb = keys( %i );
print STDERR "$nfb unique InChIKey first block values\n";

print STDERR "  Checking for missing InChIKey...\n";
my $nmiss = 0;
foreach my $cid ( keys( %v ) ) {
  if ( ! defined( $ii{ $cid } ) ) {
    print STDERR "    parent CID $cid (",
                 join( " ", sort( keys( %{ $v{ $cid } } ) ) ), ") missing InChIKey\n";
    $nmiss++;
  }
}
if ( $nmiss != 0 ) { print STDERR "    $nmiss parent CIDs are missing InChIKey!\n"; }

print STDERR "  Checking for many InChIKey to one parent CID associations...\n";
my $nmult = 0;
foreach my $cid ( keys( %ii ) ) {
  my $n = keys( %{ $ii{ $cid } } );
  if ( $n != 1 ) {
    print STDERR "    parent CID $cid  has $n InChIKey first blocks (",
                 join( " ", sort( keys( %{ $ii{ $cid } } ) ) ), ")\n";
    $nmult++;
  }
}
if ( $nmult != 0 ) { print STDERR "    $nmult parent CIDs have multiple InChIKey first blocks!\n"; }


print STDERR "Creating merged fingerprint for parent CID...\n";
foreach my $pid ( keys( %v ) ) {
  my @cids = keys( %{ $v{ $pid } } );
  my $ncids = @cids;
  if ( $ncids == 1 ) {
    $uu{ $pid } = $u{ $cids[0] };
    next;  # Nothing to merge .. just one CID
  }

  my %fp = ();
  my @fp = ();
  foreach my $cid ( @cids ) {
    $fp{ $u{ $cid } } = "";
    push( @fp, $u{ $cid } );
  }

  my $nfp = keys( %fp );
  if ( $nfp == 1 ) {
    $uu{ $pid } = $fp[ 0 ];
    next;  # Nothing to merge .. just one unique FP
  }

  print "FPs to merge:\n";
  for ( my $i = 0;  $i < $ncids;  $i++ ) {
    print "  $cids[$i]\t$fp[$i]\n";
  }
  @fp = keys( %fp );

  my $fp = "";
  my $nl = length( $fp[ 0 ] );
print "FP length: $nl\nFP count: $nfp\n";

  for ( my $j = 0; $j < $nl;  $j++ ) {
    my $v = "0";
    for ( my $i = 0; $i < $nfp;  $i++ ) {
      my $tv = substr( $fp[ $i ], $j, 1 );
      if ( $tv eq "1" ) {
        $v = "1";
        last;
      }
    }
    $fp .= $v;
  }

  $uu{ $pid } = $fp;
print "Merged FP:  $fp\n";
}
print STDERR "$nfp merged FP by parent CID\n";

print STDERR "Creating merged fingerprint for InChIKey first block...\n";
foreach my $i ( keys( %i ) ) {
  my @fp = ();
  foreach my $pid ( keys( %{ $i{ $i } } ) ) {
    push( @fp, $uu{ $pid } );
  }

  my $fp = "";
  my $nl = length( $fp[ 0 ] );
  my $n = @fp;
  for ( my $i = 0; $i < $n;  $i++ ) {
    my $v = "0";
    for ( my $j = 0; $j < $nl;  $j++ ) {
      my $tv = substr( $fp[ $i ], $j, 1 );
      if ( $tv eq "1" ) {
        $v = "1";
        last;
      }
      $fp .= $v;
    }
  }

  $uuu{ $i } = $fp;
}
my $nfp = keys( %uuu );
print STDERR "$nfp merged FP by InChIKey first block\n";


print STDERR "Outputing final tabular content by InChIKey first block...\n";
foreach my $i ( keys( %i ) ) {
  # Check if more than one associated parent CIDs (if so, pick one)
  my @pcid = keys( %{ $i{ $i } } );
  my $npcid = @pcid;
  my $pid = 0;
  if ( $npcid == 0 ) {
    print STDERR "InChI block \"$i\" missing parent CID!\n";
    next;
  } else {
    $pid = $pcid[ 0 ];
  }

  my $fp = "";
  if ( defined( $uuu{ $i } ) ) {
    $fp = $uuu{ $i };
  } else {
    print STDERR "InChI block \"$i\" missing fingerprint!\n";
    next;
  }

  my $npmid = 0;
  if ( defined( $ppp{ $i } ) ) {
    $npmid = keys( %{ $ppp{ $i } } );
  }

  my $nppid = 0;
  if ( defined( $ooo{ $i } ) ) {
    $nppid = keys( %{ $ooo{ $i } } );
  }

  my %b = ();
  foreach my $tpid ( @pcid ) {
    foreach my $tcid ( keys( %{ $v{ $tpid } } ) ) {
      $b{ $tcid } = "";
    }
  }
  my $ncids = keys( %b );
  if ( $ncids == 0 ) {
    print STDERR "InChI block \"$i\" missing CIDs!\n";
    next;
  }
  my $cids = join( " ", sort bynum( keys( %b ) ) );

  print "$pid\t$i\t$fp\t$npmid\t$nppid\t$cids\n";
}

sub bynum { $a <=> $b };

